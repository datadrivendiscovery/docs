#!/usr/bin/env bash
# Build docs for given ref

REPO_URL=$1
REF=$2
VERSIONS_FILE=$3
PUBLIC_PATH=$4
REPO_PATH="d3m"
set -e

if [ -d "$REPO_PATH" ]; then
    echo "$REPO_PATH already exists, deleting..."
    rm -rf $REPO_PATH
fi
git clone $REPO_URL $REPO_PATH
pushd $REPO_PATH
git fetch --all --tags
git checkout $REF

# Check if conf.py exists
if [ ! -f "docs/conf.py" ]; then
    echo "Config file conf.py not found under docs/ exiting"
    echo "Deleting cloned repo"
    rm -rf $REPO_PATH
    exit 0
fi
popd
echo $REF >> $VERSIONS_FILE
# Create virtualenv to install current tag, since requirements may change from version to version
virtualenv --system-site-packages $REF
source $REF/bin/activate
pushd $REPO_PATH
pip3 install pip==18.1
pip3 install --process-dependency-links -e .
# We have to use sphinx with python inside the virtualenv
# so that sphinx can import d3m package inside the virtualenv.
python3 "$(which sphinx-apidoc)" --no-toc --separate --module-first -o docs d3m
python3 "$(which sphinx-build)" docs $PUBLIC_PATH/$REF/
deactivate
echo "Deleting cloned repo $REPO_PATH"
rm -rf $REPO_PATH
echo "Deleting virtual env $REF"
rm -rf $REF
popd
